<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('pages.index');
});
Route::get('/layout/master', function () {
    return view('layout.master');
});
Route::get('/form', function () {
    return view('pages.form');
});
Route::get('/table', function () {
    return view('tables.table');
});
Route::get('/data-table', function () {
    return view('tables.data-table');
});

Route::get('/cast', 'CastController@index');
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::delete('/cast/{cast_id}', 'CastController@destroy');
